import './App.css';

import Game from './components/Game'

function App() {
  localStorage.setItem('X',0);
  localStorage.setItem('O',0);
  return (
    <Game/>

  );
}

export default App;
