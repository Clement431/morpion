import { render, screen, fireEvent } from '@testing-library/react';
import App from './App';
import { calculateWinner } from './helper';

test('Test click on board', () => {
  render(<App />);
  const linkElement = screen.getAllByRole('button')
  fireEvent.click(linkElement[0])
  expect(linkElement[0].innerHTML).toEqual('O');
  let found = linkElement.find(element=> element.innerHTML == 'X')
  expect(found).toBeInTheDocument();
});

test('Test win', () => {
  let board = ["O", "O", "O", "", "", "", "", "", "", "",]
  expect(calculateWinner(board)).toEqual('O');

  board = ["X", "X", "X", "", "", "", "", "", "", "",]
  expect(calculateWinner(board)).toEqual('X');
});


test('Test reset button', () => {

  render(<App />);
  const linkElement = screen.getAllByRole('button')
  fireEvent.click(linkElement[0])
  const reset = screen.getByText("reset")
  fireEvent.click(reset)
  expect(linkElement[0].innerHTML).toEqual('');
});