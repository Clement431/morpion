import React, { useEffect, useState } from "react";
import { calculateWinner } from "../helper";
import Board from "./Board";

const Game = () => {

  const [grill, setGrille] = useState(Array(9).fill(null));
  const [xIsNext, setXisNext] = useState(true);
  const winner = calculateWinner(grill);
  const [nbXVictory, setBbXVictory] = useState()
  const [nbOVictory, setBbOVictory] = useState()


  useEffect(()=> {
    if (winner != null) { 
    let attempts = parseInt(localStorage.getItem(winner));
      attempts++
      localStorage.setItem(winner,attempts);
      setBbXVictory(localStorage.getItem('X'))
      setBbOVictory(localStorage.getItem('O'))
    }
  },[winner])

  const handleClick = (i) => {
    const squares = grill;
    if (winner || squares[i]) return;
    squares[i] = 'O';

      let random = Math.floor(Math.random() * squares.length)
      if (squares.includes(null)) {
        while (squares[random] !== null ) {
          random = Math.floor(Math.random() * squares.length)
        }
        squares[random] = 'X';
        setXisNext(!xIsNext);
      }
      setGrille(squares)
  };

  const reset = () => {
    setGrille(Array(9).fill(null))
  };

  return (
    <>
      <h1>Morpion</h1>
      <Board squares={grill} onClick={handleClick} />
      <div className="info-wrapper">
        <div>
          <h3>History</h3>
          <button onClick={() => reset()}>reset</button>
        </div>
        <h3>{winner ? "Winner: " + winner : "Next Player: "}</h3>
        <div id="score">
          <div>Nombre de victoir du O: {nbOVictory}</div>
          <div>Nombre de victoir du X: {nbXVictory} </div>
        </div>
     
      </div>
    </>
  );
};

export default Game;
